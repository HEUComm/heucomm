---
layout: home
title: 哈尔滨工程大学信息与通信工程学院薪火答疑团
permalink: /
---


<!-- # 哈尔滨工程大学信息与通信工程学院薪火答疑团 -->
## 说明

  - 本文档为哈尔滨工程大学信息与通信工程学院薪火答疑团建立的推免信息共享平台，由各届推免学生自发维护更新，不代表任何官方立场。
  
  - 截至2024年1月，薪火答疑团设有[b站][1]、Q群、微信公众号、[gitee仓库][2]四处答疑资料发布积累点，欢迎关注并就疑问或建议进行留言。效率最高的咨询方式为直接Q群私聊礼貌咨询推免至目标院校/导师的学长学姐，咨询前请确保已阅读对应经验分享材料，对问题有基本的了解。
  
  - 为避免浏览器缓存导致的更新不可见，若页面未显示最新内容，可在相应页面尝试 **`ctrl+F5强制刷新`**。

  - 关于本文档发现任何问题（错误、不恰当表述、自己的建议等等）请随时联系群聊管理员，热烈欢迎大家一起完善维护这份文档

  
## 结构
本文档主要包含如下内容
  - [推免概念与途径][4]
     - 推免的概念
       - 推免是什么
       - 冬令营
       - 夏令营
       - 预推免
       - 联系老师
     - 推免的途径
       - 冬令营
       - 夏令营
       - 预推免
       - 联系老师
     - 历年的学院推免指标
       - 2017级
       - 2018级
       - 2019级
       - 2020级
  - [成绩、科研、竞赛、英语等因素的重要性分析][15]
  - [材料准备][5]
     - 保研过程中可能用到的材料
     - 1，个人简历
     - 2，证件照、身份证、学生证等扫描件
     - 3，成绩单与排名证明
     - 4，个人陈述
     - 5，四六级证明
     - 6，推荐信
     - 7，竞赛获奖（参加）证书、奖学金证书等扫描文件
     - 8，论文证明
     - 9，项目证明
  - [研究生方向选择][6]
  - [课题组与导师的选择与联系][8]
     - 个人定位
       - 学校、学院、学专硕、工程硕博专项
     - 导师学术水平
     - 导师人品
     - 联系导师
     - 报名通知搜集（学院/课题组）
     - 套磁
  - [面试复习][9]
     - 一般面试形式
     - 复习准备 
  - 学校分析与面试题
     - [国防科技大学][10]
     - [电子科技大学][11]
     - [东南大学][12]
     - [西安电子科技大学][13]
     - [哈尔滨工业大学][14]
     - [清北华五][17]
     - [其他学校][18]


## 致谢
  - 我们曾经都是学弟学妹，我们也都会是学长学姐。感谢所有分享资料与经验的哈工程信通学院校友，是你们的无私让本就不该存在的信息差消解，让一级一级的推免旅程更加清晰光明。


## 声明
本仓库采用[Jekyll Gitbook][3]模板。模板的选定受到[此仓库][16]启发，一并感谢



<!-- 外链 -->
[1]: https://space.bilibili.com/342666132?spm_id_from=333.337.search-card.all.click
[2]: https://gitee.com/zzl020311/heucomm/tree/master
[3]: https://github.com/sighingnow/jekyll-gitbook
[16]: https://gitee.com/li-zch/learn-in-heu
<!-- 材料准备 -->
[4]: http://heucomm.gitee.io/heucomm/pages/2023-10-20-%E6%8E%A8%E5%85%8D%E6%A6%82%E5%BF%B5%E4%B8%8E%E9%80%94%E5%BE%84/
[15]: http://heucomm.gitee.io/heucomm/pages/2023-10-21-%E6%88%90%E7%BB%A9%E3%80%81%E7%A7%91%E7%A0%94%E3%80%81%E7%AB%9E%E8%B5%9B%E3%80%81%E8%8B%B1%E8%AF%AD%E7%AD%89%E5%9B%A0%E7%B4%A0%E7%9A%84%E9%87%8D%E8%A6%81%E6%80%A7%E5%88%86%E6%9E%90/
[5]: http://heucomm.gitee.io/heucomm/pages/2023-10-22-%E6%9D%90%E6%96%99%E5%87%86%E5%A4%87/
[6]: http://heucomm.gitee.io/heucomm/pages/2023-10-23-%E7%A0%94%E7%A9%B6%E7%94%9F%E6%96%B9%E5%90%91%E9%80%89%E6%8B%A9/
[8]: https://heucomm.gitee.io/heucomm/pages/2023-10-25-%E8%AF%BE%E9%A2%98%E7%BB%84%E4%B8%8E%E5%AF%BC%E5%B8%88%E7%9A%84%E9%80%89%E6%8B%A9%E4%B8%8E%E8%81%94%E7%B3%BB/
[9]: http://heucomm.gitee.io/heucomm/pages/2023-10-26-%E9%9D%A2%E8%AF%95%E5%A4%8D%E4%B9%A0/
<!-- 学校资料 -->
[10]: http://heucomm.gitee.io/heucomm/jekyll/2023-10-17-%E5%9B%BD%E9%98%B2%E7%A7%91%E6%8A%80%E5%A4%A7%E5%AD%A6.html
[11]: http://heucomm.gitee.io/heucomm/jekyll/2023-10-18-%E7%94%B5%E5%AD%90%E7%A7%91%E6%8A%80%E5%A4%A7%E5%AD%A6.html
[12]: http://heucomm.gitee.io/heucomm/jekyll/2023-10-19-%E4%B8%9C%E5%8D%97%E5%A4%A7%E5%AD%A6.html
[13]: http://heucomm.gitee.io/heucomm/jekyll/2023-10-20-%E8%A5%BF%E5%AE%89%E7%94%B5%E5%AD%90%E7%A7%91%E6%8A%80%E5%A4%A7%E5%AD%A6.html
[14]: http://heucomm.gitee.io/heucomm/jekyll/2023-10-21-%E5%93%88%E5%B0%94%E6%BB%A8%E5%B7%A5%E4%B8%9A%E5%A4%A7%E5%AD%A6.html
[17]: https://heucomm.gitee.io/heucomm/jekyll/2023-10-25-%E6%B8%85%E5%8C%97%E5%8D%8E%E4%BA%94.html
[18]: https://heucomm.gitee.io/heucomm/jekyll/2023-10-26-%E5%85%B6%E4%BB%96%E5%AD%A6%E6%A0%A1.html