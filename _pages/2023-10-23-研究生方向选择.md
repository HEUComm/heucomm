---
title: 研究生方向选择
author: HEUComm
date: 2023-10-23
category: Jekyll
layout: post
---

请先观看：

[保研第十讲：保研流程指导+通信方向热门学校实力评估（中科院、东南、哈工大、哈工程、北理、北航、北邮、成电、西电、国防科大）][1]

[保研第十一讲：1.确定学校与研究方向 2.联系导师 3.面试经验 4.推免系统填报][2]

---

对于我们信息与通信工程的同学来说，升学时可选择的较为对口的方向较多，如：

雷达等信号处理、微波（波导、材料、微波芯片等）、信息与通信工程、集成电路、ai+通信、ai、计算机学院、自动化学院、精密仪器学院、海洋方向（水下机器人，水下信号处理等）……

分别介绍如下：

---
此外，直博、学硕、专硕、工程硕博士培养改革专项也十分关键，具体介绍如下：


---
本文档贡献者：

2020级：



[1]: https://www.bilibili.com/video/BV1c3411s7ub/?spm_id_from=333.999.0.0&vd_source=1b6fa86955ab8435caf72450d2f004e2
[2]: https://www.bilibili.com/video/BV1Hq4y1v7mu/?spm_id_from=333.999.0.0&vd_source=1b6fa86955ab8435caf72450d2f004e2
